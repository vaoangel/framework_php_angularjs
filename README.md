# Project Angularjs 1.4 PHP framework


## Technologies

HTML

CSS

Angularjs 1.4

PHP

MySQL



## Functionality

```bash
Modules: Contact, CRUD, Home, Login / Social_login, Shop, Profile
```
## Functionality by module
```bash
Contact: mail contact with Mailgun.

CRUD: Module for users Administrators with the possibility to create, modify or delete products

Home: main module with a carrousel images with a list of paginated products and autocomplete

Login: Register form and login and password recovery section / Social login

Shop: Table reservation system in the desired product

Profile: Modify profile data, list the reserves, see the user's data.

```

## Code improvements
Dropzone and datepicker global directive.

Generated token by JWT.

Renewal of token per user for each new entry in each module and every certain period of time.

Loginservice.

LocalStorageService.

PHP Framework (Controller-Model-BLL-DAO).

Multi-user table in BD.

Geolocalization of products by Google Maps.

Details of the products

Dependent dropdown.

Product pagination

Autosearch Autocomplete of products.

Scroll on button.

Recover password with confirmation by mail

Activation of user by mail

Social login by AUTH0.

3 updates of the token each time it enters a social login.

Product rating system.

Transactions in BDD









## Author
Àngel Vañó Francés.