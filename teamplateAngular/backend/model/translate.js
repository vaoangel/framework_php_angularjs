
frases = {
    "en": {
        
      "Cambiar idioma:": "Change language:",
      "Castellano": "Spanish",
      "Inglés": "English",
      "Valenciano": "Valencian",
      "Inicio": "Start Page",
      "Home": "Home page",
      "Reservas": "Reserve",
      "Servicios": "Services",
      "Conocenos": "Know about us",  
      "Contacto": "Contact",
      "Nombre": "Name",
      "Apellidos": "Surname",
      "numpeople": "Number of people",
      "Email": "Email",
      "menu" : "Menu",
      "alergens" : "Allergens",
      "fecha" : "Reservation date",
      "enviar" : "Send",
      "Order" : "Order",
      "Tienda": "Shop",
      "password" : "Password",
      "Asumpte": "Problem",
      "Usuario": "User",

      },
    "val": {
        "Cambiar idioma:": "Canviar llengua",
      "Castellano": "Castella",
      "Inglés": "Anglés",
      "Valenciano": "Valencià",
      "Inicio": "Portada",
      "Home": "Pag principal",
      "Reservas": "Reserves",
      "Servicios": "Servicis",
      "Conocenos": "Coneix-nos",
      "Contacto": "Contactans",
      "Nombre": "Nom",
      "Apellidos" : "Cognoms",
      "numpeople": "Nombre de persones",
      "Email": "Correu electrònic",
      "menu" : "Menú",
      "alergens" : "Alèrgens",
      "fecha" : "Data de reserva",
      "enviar" : "Finalitzar",
      "Order" : "Ordernar",
      "Tienda": "Tenda",
      "password": "Contrasenya",
      "Asumpte" : "Asumpte",
      "Usuario": "Usuari"

    },
    "cas": {
        "Cambiar idioma:": "Cambiar idioma:",
        "Castellano": "Castellano",
        "Inglés": "Inglés",
        "Valenciano": "Valenciano",
        "Inicio": "Portada",
        "Home": "Pag principal",
        "Reservas": "Reservas",
        "Servicios": "Servicios",
        "Conocenos": "Conocenos",
        "Contacto": "Contacto",
        "Nombre": "Nombre",
        "Apellidos": "Apellidos",
        "numpeople": "Número de personas",
        "Email": "Correo electrónico",
        "menu" : "Menú",
        "alergens" : "Alérgenos",
        "enviar" : "Enviar",
        "Order" : "Ordernar",
        "Tienda": "Tienda",
        "password": "Contraseña",
        "Asumpte" : "Asunto",
        "Usuario": "Usuario"
    }
  };
  
  /**
   * Función que cambia todos los elementos al nuevo idioma.
   *
   * @param {string} lang
   */
  function cambiarIdioma(lang) {
  
     lang = lang || sessionStorage.getItem('app-lang') || 'es';
     sessionStorage.setItem('app-lang', lang);
  
    var elems = document.querySelectorAll('[data-tr]');
    for (var x = 0; x < elems.length; x++) {
      elems[x].innerHTML = frases.hasOwnProperty(lang)
        ? frases[lang][elems[x].dataset.tr]
        : elems[x].dataset.tr;
    }
  }
  
  window.onload = function(){
    cambiarIdioma();
    
    document
      .getElementById('btn-cas')
      .addEventListener('click', cambiarIdioma.bind(null, 'cas'));
  
    document
      .getElementById('btn-en')
      .addEventListener('click', cambiarIdioma.bind(null, 'en'));
  
    document
      .getElementById('btn-val')
      .addEventListener('click', cambiarIdioma.bind(null, 'val'));
  }