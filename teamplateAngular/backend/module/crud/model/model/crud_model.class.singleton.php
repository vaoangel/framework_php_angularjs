<?php
//echo json_encode("products model class");
//exit;

require(SITE_ROOT . "module/crud/model/BLL/crud_bll.class.singleton.php");

class crud_model
{
    private $bll;
    static $_instance;

    private function __construct()
    {
        $this->bll = crud_bll::getInstance();
    }

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function insert_product($data)
    {

        return $this->bll->insert_product_BLL($data);
    }
    public function update_product($data)
    {

        return $this->bll->update_product_BLL($data);
    }
    public function delete_product($id)
    {

        return $this->bll->delete_product_BLL($id);
    }

    public function delete_product_error($data){
        return $this->bll->delete_product_error_BLL($data);

    }
    public function insert_rating($data){
        return $this->bll->insert_rating_BLL($data);

    }
}
