<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

// $path = $_SERVER['DOCUMENT_ROOT'] . '/1DAW/teamplate/';
// if(!defined('SITE_ROOT')) define('SITE_ROOT', $path);
// if(!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'model/');
require(MODEL_PATH . "DB.class.singleton.php");
require(SITE_ROOT . "module/crud/model/DAO/crud_dao.class.singleton.php");

class crud_bll
{
  private $dao;
  private $db;
  static $_instance;

  private function __construct()
  {
    $this->dao = crudDAO::getInstance();
    $this->db = Db::getInstance();
  }

  public static function getInstance()
  {
    if (!(self::$_instance instanceof self)) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

  public function insert_product_BLL($data)
  {
    return $this->dao->insert_product_DAO($this->db, $data);
  }
  public function update_product_BLL($data)
  {
    return $this->dao->update_product_DAO($this->db, $data);
  }
  public function delete_product_BLL($id)
  {
    return $this->dao->delete_product_DAO($this->db, $id);
  }

  public function delete_product_error_BLL($data){
    return $this->dao->delete_product_error_DAO($this->db, $data);

  }

  public function insert_rating_BLL($data){
    return $this->dao->insert_rating_DAO($this->db, $data);

  }
}
