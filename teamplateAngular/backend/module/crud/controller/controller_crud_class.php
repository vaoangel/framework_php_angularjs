<?php
@session_start();
class controller_crud
{
    function __construct()
    {
        //include(UTILS_PRODUCTS . "utils.inc.php");

        $_SESSION['module'] = "crud";
    }

    public function insert_product()
    {
        $info_data = json_decode($_POST['total_data'], true);
        // echo json_encode($info_data);

        $arrArgument = array(
            'nombre' => $info_data['nombre'],
            'provincia' => $info_data['provincia'],
            'poblacion' => $info_data['poblacion'],
            'imagen' => $info_data['imagen'],
            'lat' => $info_data['lat'],
            'long' => $info_data['long'],
            'calidad' => $info_data['calidad'],
            'descr' => $info_data['descr'],
            'user' => $info_data['user']


        );
        $resultado = loadModel(MODEL_CRUD, "crud_model", "insert_product", $arrArgument);
        $rating = loadModel(MODEL_CRUD, "crud_model", "insert_rating", $arrArgument);
        if($resultado == false){

            $resultadoDel = loadModel(MODEL_CRUD, "crud_model", "delete_product_error", $arrArgument);

        }
         echo json_encode($resultado);
    }

    public function update_product(){
        $info_data = json_decode($_POST['total_data'], true);
        // echo json_encode($info_data);

        $arrArgument = array(
            'restaurante' => $info_data['restaurant'],
            'provincia' => $info_data['provincia'],
            'poblacion' => $info_data['poblacion'],
            'imagen' => $info_data['imagen'],
            'lat' => $info_data['lat'],
            'long' => $info_data['long'],
            'calidad' => $info_data['calidad'],
            'descr' => $info_data['descr'],

        );

        $resultado = loadModel(MODEL_CRUD, "crud_model", "update_product", $arrArgument);
        echo json_encode($resultado);

    }

    public function delete_product(){
        $info_data = json_decode($_POST['total_data'], true);
        $resultado = loadModel(MODEL_CRUD, "crud_model", "delete_product", $info_data['id']);
        echo json_encode($resultado);

    }
}
