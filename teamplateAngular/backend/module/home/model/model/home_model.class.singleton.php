<?php
//echo json_encode("products model class");
//exit;

require(SITE_ROOT . "module/home/model/BLL/home_bll.class.singleton.php");

class home_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = home_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function details_products($id){
        return $this->bll->details_products_BLL($id);
    }
    public function select_restaurants($arrArgument){
        return $this->bll->select_restaurants_BLL($arrArgument);
    
    }
    public function count_restaurants($arrArgument){
        return $this->bll->count_restaurants_BLL($arrArgument);
    
    }

    public function insert_reserve($data){
        return $this->bll->insert_reserve_BLL($data);

    }
    public function active_user($data){
        return $this->bll->active_user_BLL($data);
    }
    public function timeout_token($data){
        return $this->bll->timeout_token_BLL($data);
    }
    public function timeout_activate($data){
        return $this->bll->timeout_activate_BLL($data);
    }
    public function timeout_tipo($data){
        return $this->bll->timeout_tipo_BLL($data);
    }

    public function token_match($data){
        return $this->bll->token_match_BLL($data);

    }
    public function update_rate($data){
        return $this->bll->update_rate_BLL($data);

    }
    public function get_rating(){
        return $this->bll->get_rating_BLL();

    }

}