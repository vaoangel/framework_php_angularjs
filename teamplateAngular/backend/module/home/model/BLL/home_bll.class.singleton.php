<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

// $path = $_SERVER['DOCUMENT_ROOT'] . '/1DAW/teamplate/';
// if(!defined('SITE_ROOT')) define('SITE_ROOT', $path);
// if(!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'model/');
require(MODEL_PATH . "DB.class.singleton.php");
require(SITE_ROOT . "module/home/model/DAO/home_dao.class.singleton.php");

class home_bll
{
  private $dao;
  private $db;
  static $_instance;

  private function __construct()
  {
    $this->dao = homeDAO::getInstance();
    $this->db = Db::getInstance();
  }

  public static function getInstance()
  {
    if (!(self::$_instance instanceof self)) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }
  public function details_products_BLL($id)
  {
    return $this->dao->details_products_DAO($this->db, $id);
  }
  public function select_restaurants_BLL($arrArgument)
  {
    return $this->dao->select_restaurants_DAO($this->db, $arrArgument);
  }
  public function insert_reserve_BLL($data)
  {
    return $this->dao->insert_reserve_DAO($this->db, $data);
  }
  public function active_user_BLL($arrArgument)
  {
    return  $this->dao->active_user_DAO($this->db, $arrArgument);
  }

  public function count_restaurants_BLL($arrArgument)
  {
    return $this->dao->count_restaurants_DAO($this->db, $arrArgument);
  }

  public function timeout_token_BLL($data)
  {
    return $this->dao->timeout_token_DAO($this->db, $data);
  }

  public function timeout_tipo_BLL($data)
  {
    return $this->dao->timeout_tipo_DAO($this->db, $data);
  }

  public function timeout_activate_BLL($data)
  {
    return $this->dao->timeout_activate_DAO($this->db, $data);
  }
  public function token_match_BLL($data)
  {
    return $this->dao->token_match_DAO($this->db, $data);
  }

  public function update_rate_BLL($data)
  {
    return $this->dao->update_rate_DAO($this->db, $data);
  }
  public function get_rating_BLL(){
    return $this->dao->get_rating_DAO($this->db);

  }


}
