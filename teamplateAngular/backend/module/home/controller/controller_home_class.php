<?php
@session_start();
include(UTILS_LOGIN . "JWT.php");

class controller_home
{
	function __construct()
	{
		//include(UTILS_PRODUCTS . "utils.inc.php");

		$_SESSION['module'] = "home";
	}


	

	public function get_restaurants()
	{
		$resultado = loadModel(MODEL_HOME, "home_model", "select_restaurants", '');
		//echo "daasdasd";
		//debugPHP($resultado);
		echo json_encode($resultado);
	}

	public function get_restaurants2()
	{
		$resultado = loadModel(MODEL_HOME, "home_model", "select_restaurants", '');
		$resultado2 = loadModel(MODEL_HOME, "home_model", "count_restaurants", '');


		$array = array();
		for ($i = 0; $i < $resultado2['0']['total']; $i++) {
			array_push($array,  $resultado[$i]['nombre']);
		}
		echo json_encode($array);
	}
	public function details_products()
	{
		//debugPHP($_GET);
		$results = loadModel(MODEL_HOME, "home_model", "details_products", $_GET['param']);
		echo json_encode($results);
	}


	public function insert_reserve()
	{
		$info_data = json_decode($_POST['total_data'], true);
		//echo json_encode($info_data);
		$arrArgument = array(
			'propietario' => $info_data['usuario'],
			'nombre_contacto' => $info_data['nombre_reserva'],
			'email' => $info_data['email'],
			'dia_reserva' => $info_data['fecha'],
			'hora_reserva' => $info_data['hora'],
			'restaurante' => $info_data['restaurante']

		);
		$result = loadModel(MODEL_HOME, "home_model", "insert_reserve", $arrArgument);

		echo json_encode($result);
		$resultado['type'] = 'rememberReserve';
		$resultado['nombre'] =  $info_data['usuario'];
		$resultado['inputEmail'] = $info_data['email'];
		$resultado['restaurante'] = $info_data['restaurante'];
		$resultado['inputMessage'] = "Su reserva se ha efectuado el dia: " + $info_data['fecha'] + " a las: " + $info_data['hora'];
		//debugPHP($resultado);
		enviar_email($resultado);
	}

	public function timeout_token()
	{
		$info_data = json_decode($_POST['total_data'], true);
		$token = encode($info_data['id_user']);

		$arrArgument = array(
			'oldtoken' => $info_data['oldtoken'],
			'newtoken' => $token,
			'id_user' => $info_data['id_user']
		);
		$result1 = loadModel(MODEL_HOME, "home_model", "timeout_tipo", $arrArgument);
		$result2 = loadModel(MODEL_HOME, "home_model", "timeout_activate", $arrArgument);
		$result3 = loadModel(MODEL_HOME, "home_model", "timeout_token", $arrArgument);

		$results = array(
			"result1" =>  $result1,
			"result2" =>  $result2,
			"result3" => $result3,
			"newtoken"  =>  $token,
			"id_user" => $info_data['id_user'],
		);

		echo json_encode($results);
	}

	public function token_match()
	{
		$info_data = json_decode($_POST['total_data'], true);
		$arrArgument = array(
			'token' => $info_data['oldtoken'],
			'id_user' => $info_data['id_user']
		);
		$result1 = loadModel(MODEL_HOME, "home_model", "token_match", $arrArgument);
		echo json_encode($result1);
	}

	public function update_rate(){
		$info_data = json_decode($_POST['total_data'], true);

		$arrArgument= array(
			'user' => $info_data['user'],
			'rate' => $info_data['rate'],
			'restaurant' =>$info_data['restaurant']
		);
		$result1 = loadModel(MODEL_HOME, "home_model", "update_rate", $arrArgument);

		echo json_encode($result1);


	}

	public function rate_prods(){
		$result1 = loadModel(MODEL_HOME, "home_model", "get_rating", '');

		echo json_encode($result1);
	}






   
}
