<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class loginDAO
{
    static $_instance;
    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function  insert_user_DAO($db, $arrArgument)
    {
        $user = $arrArgument['nombre'];
        $password = $arrArgument['password'];
        $avatar = $arrArgument['avatar'];

        $email = $arrArgument['email'];
        $sql = "INSERT INTO `user`  VALUES ('$user', '$user','$email','$password','$avatar','not_set','not_set')";
        //echo $sql;
        return $db->ejecutar($sql);
    }
    public function  insert_token_DAO($db, $arrArgument)
    {
        $user = $arrArgument['nombre'];
        $token = $arrArgument['token'];

        $sql = "INSERT INTO `token` VALUES ('$user','$token')";
        return $db->ejecutar($sql);
    }
    public function  insert_tipo_DAO($db, $arrArgument)
    {
        $token = $arrArgument['token'];
        $sql =  "INSERT INTO `tipo` VALUES ('$token','unset')";
        return $db->ejecutar($sql);
    }
    public function  insert_activate_DAO($db, $arrArgument)
    {
        $token = $arrArgument['token'];
        $activate = $arrArgument['activate'];
        $sql = "INSERT INTO `activate` VALUES ('$token',' $activate')";
        return $db->ejecutar($sql);
    }

    public function get_token_DAO($db, $arrArgument)
    {

        $sql = "SELECT token from token where id_user like '$arrArgument'";
        $stmt = $db->ejecutar($sql);
        //echo $stmt;
        return $db->listar($stmt);
    }

  

    public function search_user_DAO($db, $arrArgument)
    {
        $sql = "SELECT * from user where id_user like '$arrArgument'";
        //$sql = "SELECT COUNT(*) as total FROM restaurantes";

        //echo $sql;
        $stmt = $db->ejecutar($sql);
        //echo $stmt;
        return $db->listar($stmt);
    }

  

    public function login_user_DAO($db, $arrArgument)
    {


        $sql = "SELECT t.token , u.password from token t, user u where u.id_user = '$arrArgument' and t.id_user= '$arrArgument'";
        //echo $sql;
        $stmt = $db->ejecutar($sql);
        //echo $stmt;
        return $db->listar($stmt);
    }





    public function search_activate_DAO($db, $arrArgument)
    {
        $sql = "SELECT a.activate , t.tipo from activate a, tipo t where a.id_user = '$arrArgument' and t.id_user= '$arrArgument'";
        $stmt = $db->ejecutar($sql);
        //echo $stmt;
        return $db->listar($stmt);
    }

    public function update_activate_DAO($db, $data)
    {
        $new_token = $data['new_token'];
        $old_token = $data['old_token'];
        $sql = "UPDATE activate set id_user = '$new_token' where id_user like '$old_token'";
        return $db->ejecutar($sql);
    }
    public function update_tipo_DAO($db, $data)
    {
        $new_token = $data['new_token'];
        $old_token = $data['old_token'];
        $sql = "UPDATE tipo set id_user = '$new_token' where id_user like '$old_token'";
        return $db->ejecutar($sql);
    }
    public function update_token_DAO($db, $data)
    {
        $new_token = $data['new_token'];
        $old_token = $data['old_token'];
        $sql = "UPDATE token set token = '$new_token' where token like '$old_token'";
        return $db->ejecutar($sql);
    }
    public function update_password_DAO($db, $data)
    {
        $password = $data['password'];
        $user = $data['user'];
        $sql = "UPDATE user set password = '$password' where id_user like '$user'";
        return $db->ejecutar($sql);
    }

    public function obtain_paises_DAO($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $file_contents = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $accepted_response = array(200, 301, 302);
        if (!in_array($httpcode, $accepted_response)) {
            return FALSE;
        } else {
            return ($file_contents) ? $file_contents : FALSE;
        }
    }
    public function obtain_provincias_DAO()
    {
        $json = array();
        $tmp = array();

        $provincias = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i += 2) {
            $e = $i + 1;
            $provincia = $result[$e];
            $tmp = array(
                'id' => (string)$result[$i], 'nombre' => (string)$provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }
    public function obtain_poblaciones_DAO($arrArgument)
    {
        $json = array();
        $tmp = array();

        $filter = (string)$arrArgument;
        $xml = simplexml_load_file(RESOURCES . 'provinciasypoblaciones.xml');
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string)$result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function update_user_DAO($db, $data)
    {
        $id_user = $data['id_user'];
        $nombre = $data['nombre'];
        $avatar = $data['avatar'];
        $ciudad = $data['ciudad'];
        $provincia = $data['provincia'];
        $sql = "UPDATE user set nombre = '$nombre', avatar = '$avatar', ciudad = '$ciudad', provincia = '$provincia' where id_user = '$id_user'";
        return $db->ejecutar($sql);
    }

    public function search_reserves_DAO($db, $data)
    {
        $sql = "SELECT * FROM reserves where propietario like '$data'";
        //echo $sql;
        $stmt = $db->ejecutar($sql);
        //echo $stmt;
        return $db->listar($stmt);
    }

    public function delete_user_DAO($db, $data)
    {
        $user = $data['nombre'];

        $sql = "DELETE from user where id_user like '$user'";

        return $db->ejecutar($sql);
    }

    public function delete_activate_DAO($db, $data)
    {
        $token = $data['token'];

        $sql = "DELETE from activate where id_user like '$token'";

        return $db->ejecutar($sql);
    }

    public function delete_tipo_DAO($db, $data)
    {
        $token = $data['token'];

        $sql = "DELETE from tipo where id_user like '$token'";

        return $db->ejecutar($sql);

     }
     public function delete_token_DAO($db, $data)
     {
         $token = $data['token'];
 
         $sql = "DELETE from token where token like '$token'";
 
         return $db->ejecutar($sql);
 
      }
     
}
