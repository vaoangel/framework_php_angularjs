<?php
//echo json_encode("products model class");
//exit;

require(SITE_ROOT . "module/login/model/BLL/login_bll.class.singleton.php");

class login_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function insert_user($data){
       
        return $this->bll->insert_user_BLL($data);   
     }

     public function insert_token($data){
         return $this->bll->insert_token_BLL($data);
     }
     public function insert_tipo($data){
        return $this->bll->insert_tipo_BLL($data);
    }
    public function insert_activate($data){
        return $this->bll->insert_activate_BLL($data);
    }
    public function search_user($data){
        return $this->bll->search_user_BLL($data);
    }

    public function get_token($data){
        return $this->bll->get_token_BLL($data);
    }

    public function login_user($data){
        return $this->bll->login_user_BLL($data);

    }
   
    public function search_activate($data){
        return $this->bll->search_activate_BLL($data);
    }
    
    public function update_tipo($data){
        return $this->bll->update_tipo_BLL($data);
    }
    public function update_token($data){
        return $this->bll->update_token_BLL($data);
    }
    public function update_activate($data){
        return $this->bll->update_activate_BLL($data);
    }

    public function update_password($data){
        return $this->bll->update_password_BLL($data);

    }
    public function obtain_paises($url) {
        return $this->bll->obtain_paises_BLL($url);
    }
    public function obtain_provincias() {
        return $this->bll->obtain_provincias_BLL();
    }
    public function obtain_poblaciones($arrArgument) {
        return $this->bll->obtain_poblaciones_BLL($arrArgument);
    }

    public function update_user($data){
        return $this->bll->update_user_BLL($data);

    }

    public function search_reserves($data){
        return $this->bll->search_reserves_BLL($data);

    }

    public function delete_user($data){
        return $this->bll->delete_user_BLL($data);

    }

    public function delete_activate($data){
        return $this->bll->delete_activate_BLL($data);
    }
    public function delete_tipo($data){
        return $this->bll->delete_tipo_BLL($data);
    }
    public function delete_token($data){
        return $this->bll->delete_token_BLL($data);
    }
}