<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

// $path = $_SERVER['DOCUMENT_ROOT'] . '/1DAW/teamplate/';
// if(!defined('SITE_ROOT')) define('SITE_ROOT', $path);
// if(!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'model/');
require(MODEL_PATH . "DB.class.singleton.php");
require(SITE_ROOT . "module/login/model/DAO/login_dao.class.singleton.php");

class login_bll
{
    private $dao;
    private $db;
    static $_instance;

    private function __construct()
    {
        $this->dao = loginDAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function insert_user_BLL($arrArgument)
    {
        return $this->dao->insert_user_DAO($this->db, $arrArgument);
    }
    public function insert_token_BLL($arrArgument)
    {
        return  $this->dao->insert_token_DAO($this->db, $arrArgument);
    }

    public function insert_tipo_BLL($arrArgument)
    {
        return  $this->dao->insert_tipo_DAO($this->db, $arrArgument);
    }
    public function insert_activate_BLL($arrArgument)
    {
        return  $this->dao->insert_activate_DAO($this->db, $arrArgument);
    }
    public function search_user_BLL($arrArgument)
    {
        return  $this->dao->search_user_DAO($this->db, $arrArgument);
    }
    public function get_token_BLL($arrArgument)
    {
        return  $this->dao->get_token_DAO($this->db, $arrArgument);
    }

    public function login_user_BLL($arrArgument)
    {
        return  $this->dao->login_user_DAO($this->db, $arrArgument);
    }
  
  


    public function search_activate_BLL($arrArgument)
    {
        return  $this->dao->search_activate_DAO($this->db, $arrArgument);
    }

    public function update_activate_BLL($data)
    {
        return $this->dao->update_activate_DAO($this->db, $data);
    }
    public function update_token_BLL($data)
    {
        return $this->dao->update_token_DAO($this->db, $data);
    }
    public function update_tipo_BLL($data)
    {
        return $this->dao->update_tipo_DAO($this->db, $data);
    }
    public function update_password_BLL($data)
    {
        return $this->dao->update_password_DAO($this->db, $data);
    }

    public function obtain_paises_BLL($url)
    {
        return $this->dao->obtain_paises_DAO($url);
    }
    public function obtain_provincias_BLL()
    {
        return $this->dao->obtain_provincias_DAO();
    }
    public function obtain_poblaciones_BLL($arrArgument)
    {
        return $this->dao->obtain_poblaciones_DAO($arrArgument);
    }

    public function update_user_BLL($data)
    {
        return $this->dao->update_user_DAO($this->db, $data);
    }
    public function search_reserves_BLL($data)
    {
        return $this->dao->search_reserves_DAO($this->db, $data);
    }

    public function delete_user_BLL($data)
    {
        return $this->dao->delete_user_DAO($this->db, $data);
    }

    public function delete_activate_BLL($data)
    {
        return $this->dao->delete_activate_DAO($this->db, $data);
    }
    public function delete_tipo_BLL($data)
    {
        return $this->dao->delete_tipo_DAO($this->db, $data);
    }
    public function delete_token_BLL($data)
    {
        return $this->dao->delete_token_DAO($this->db, $data);
    }
}
