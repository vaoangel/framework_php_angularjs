<?php
//require_once '/opt/lampp/htdocs/1DAW/teamplateAngular/backend/module/login/utils/JWT.php';
include(UTILS_LOGIN . "JWT.php");

class controller_login
{
	function __construct()
	{
		//include(UTILS_PRODUCTS . "utils.inc.php");
		$_SESSION['module'] = "login";
	}


	public function validate_login()
	{
		$info_data = json_decode($_POST['total_data'], true);

		$activate = loadModel(MODEL_LOGIN, 'login_model', 'search_user', $info_data['luser']);
		if ($activate) {
			//echo json_encode('siexiste');

			$getpasswd = loadModel(MODEL_LOGIN, 'login_model', 'login_user', $info_data['luser']);

			if (password_verify($info_data['lpasswd'], $getpasswd[0]['password'])) {
				$confirm_activate = loadModel(MODEL_LOGIN, 'login_model', 'search_activate', $getpasswd[0]['token']);
				// debugPHP($confirm_activate);
				if ($confirm_activate[0]['activate'] == 1) {

					$arrArgument = array(
						'id_user' => $info_data['luser'],
						'token' => $getpasswd[0]['token'],
						'tipo' => $confirm_activate[0]['tipo'],

					);
					echo json_encode($arrArgument);
				} else {
					echo json_encode('inactivo');
				}
			} else {
				echo json_encode('wrongpass');
			}
		} else {
			echo json_encode('noexiste');
		}
	}

	public function validate_register()
	{

		$info_data = json_decode($_POST['total_data'], true);
		// debugPHP($info_data);
		$opciones = [
			'cost' => 12,
		];

		$avatar1 = $info_data['remail'];
		$avatar = "https://api.adorable.io/avatars/80/$avatar1";
		$password =  password_hash($info_data['rpasswd'], PASSWORD_BCRYPT, $opciones);
		$token = encode($info_data['ruser']);
		$arrArgument = array(
			"nombre" => $info_data['ruser'],
			"avatar" => $avatar,
			"email"  => $info_data['remail'],
			"password" => $password,
			"token" => $token,
			"activate" => '0'
		);

		$result = loadModel(MODEL_LOGIN, 'login_model', 'search_user', $info_data['ruser']);


		// debugPHP($result);
		if (!$result) {

			$insert_user_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_user', $arrArgument);
			$insert_activate_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_activate', $arrArgument);
			$insert_type_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_tipo', $arrArgument);
			$insert_token_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_token', $arrArgument);
		
			$insert_user_table = false;
			$insert_activate_table = false;
			$insert_token_table = false;
			$insert_type_table = false;
			if ($insert_user_table == false) {
				$delete_user_table = loadModel(MODEL_LOGIN, 'login_model', 'delete_user', $arrArgument);
				echo json_encode("Peta tabla user");
			}
			if ($insert_activate_table == false) {
				$delete_activate_table = loadModel(MODEL_LOGIN, 'login_model', 'delete_activate', $arrArgument);
				echo json_encode("Peta tabla activate");
			}
			if ($insert_token_table == false) {
				$delete_token_table = loadModel(MODEL_LOGIN, 'login_model', 'delete_token', $arrArgument);
				echo json_encode("Peta tabla token");
			}
			if ($insert_type_table == false) {
				$delete_type_table = loadModel(MODEL_LOGIN, 'login_model', 'delete_tipo', $arrArgument);
				echo json_encode("Peta tabla tipo");
			}
			

			

			// $results = array(
			//     "result1" =>  true,
			//     "result2" =>  false,
			//     "result3" => false,
			//     "result4" => true,
			// );

			$results = array(
				"result1" =>  $insert_user_table,
				"result2" =>  $insert_activate_table,
				"result3" => $insert_type_table,
				"result4" => $insert_token_table,
				"token"  =>  $token,
				"id_user" => $info_data['ruser'],
			);
			$resultado['type'] = 'alta';
			$resultado['token'] = $token;
			$resultado['nombre'] =  $info_data['ruser'];
			$resultado['inputEmail'] = $info_data['remail'];
			$resultado['inputMessage'] = 'Para activar tu cuenta en mi web  pulse en el siguiente enlace';

			enviar_email($resultado);
			echo json_encode($results);
		} else {
			echo json_encode("siexiste");
		}
	}


	public function social_login()
	{
		$info_data = json_decode($_POST['total_data'], true);
		//debugPHP($info_data);
		$result = loadModel(MODEL_LOGIN, 'login_model', 'search_user', $info_data['user']);

		if (!$result) {
			$arrArgument = array(
				"nombre" => $info_data['user'],
				"avatar" => $info_data['avatar'],
				"email"  => $info_data['email'],
				"password" => "Social_login",
				"token" => $info_data['token'],
				"activate" => '0'
			);

			$insert_user_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_user', $arrArgument);
			$insert_activate_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_activate', $arrArgument);
			$insert_type_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_tipo', $arrArgument);
			$insert_token_table = loadModel(MODEL_LOGIN, 'login_model', 'insert_token', $arrArgument);

			$results = array(
				"result1" =>  $insert_user_table,
				"result2" =>  $insert_activate_table,
				"result3" => $insert_type_table,
				"result4" => $insert_token_table,
				"token"  =>  $info_data['token'],
				"id_user" => $info_data['user'],
				"Social" => "si",
			);

			$resultado['type'] = 'alta';
			$resultado['token'] = $info_data['token'];
			$resultado['nombre'] =  $info_data['user'];
			$resultado['inputEmail'] = $info_data['email'];
			$resultado['inputMessage'] = 'Para activar tu cuenta en mi web  pulse en el siguiente enlace';

			enviar_email($resultado);
			echo json_encode($results);
		} else {
			$old_token = loadModel(MODEL_LOGIN, 'login_model', 'get_token', $info_data['user']);
			$arrUpdate = array(
				'old_token' => $old_token[0]['token'],
				'new_token' => $info_data['token']
			);
			$update_token = loadModel(MODEL_LOGIN, 'login_model', 'update_token', $arrUpdate);
			$update_tipo = loadModel(MODEL_LOGIN, 'login_model', 'update_tipo', $arrUpdate);
			$update_activate = loadModel(MODEL_LOGIN, 'login_model', 'update_activate', $arrUpdate);
			$get_token = loadModel(MODEL_LOGIN, 'login_model', 'get_token', $info_data['user']);
			$get_tipo = loadModel(MODEL_LOGIN, 'login_model', 'search_activate', $info_data['token']);

			// $confirm_activate = loadModel(MODEL_LOGIN ,'login_model','search_activate',$info_data['token']);
			// $arrArgument = array (
			//     'id_user' => $info_data['user'],
			//     'token' => $info_data['token'],
			//     'tipo' => $confirm_activate['tipo'],

			// );
			$results_update = array(
				"result1" =>  $update_token,
				"result2" =>  $update_tipo,
				"result3" => $update_activate,
				"token" => $get_token[0]['token'],
				"tipo" =>  $get_tipo[0]['tipo']
			);
			echo json_encode($results_update);
		}
	}

	public function recover_passwd()
	{

		$info_data = json_decode($_POST['total_data'], true);
		echo json_encode($info_data);
		$email = loadModel(MODEL_LOGIN, 'login_model', 'search_user', $info_data['user']);
		debugPHP($email);
		$resultado['type'] = 'changepass';
		$resultado['token'] = $info_data['token'];
		$resultado['inputEmail'] = $email[0]['email'];
		$resultado['inputMessage'] = 'Para reestablecer  tu contraseña en mi web  pulse en el siguiente enlace';

		enviar_email($resultado);
	}

	public function recover_passwd_in()
	{
		$info_data = json_decode($_POST['total_data'], true);
		// echo json_encode($info_data);
		$opciones = [
			'cost' => 12,
		];


		$password =  password_hash($info_data['password'], PASSWORD_BCRYPT, $opciones);
		$arrArgument = array(
			'user' => $info_data['id_user'],
			'password' =>  $password
		);
		$result = loadModel(MODEL_LOGIN, 'login_model', 'update_password', $arrArgument);
		echo json_encode($result);
	}
	public function search_user()
	{

		$data = $_GET['param'];
		$result = loadModel(MODEL_LOGIN, 'login_model', 'search_user', $data);
		echo json_encode($result);
	}
	function upload_avatar()
	{
		$result_avatar = upload_files();
		$_SESSION['avatar'] = $result_avatar;
		echo json_encode($result_avatar);
	}
	function load_pais_login()
	{
		if ((isset($_GET["param"])) && ($_GET["param"] == true)) {
			$json = array();

			$url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
			// set_error_handler('ErrorHandler');
			try {
				$json = loadModel(MODEL_LOGIN, "login_model", "obtain_paises", $url);
			} catch (Exception $e) {
				$json = false;
			}
			//  restore_error_handler();

			if ($json) {
				echo $json;
				exit;
			} else {
				$json = "error";
				echo $json;
				exit;
			}
		}
	}
	function load_provincias_login()
	{
		if ((isset($_GET["param"])) && ($_GET["param"] == true)) {
			$jsondata = array();
			$json = array();

			//set_error_handler('ErrorHandler');
			try {
				$json = loadModel(MODEL_LOGIN, "login_model", "obtain_provincias");
			} catch (Exception $e) {
				$json = false;
			}
			restore_error_handler();

			if ($json) {
				$jsondata["provincias"] = $json;
				echo json_encode($jsondata);
				exit;
			} else {
				$jsondata["provincias"] = "error";
				echo json_encode($jsondata);
				exit;
			}
		}
	}
	function load_poblaciones_login()
	{

		$jsondata = array();
		$json = array();

		$json = loadModel(MODEL_LOGIN, "login_model", "obtain_poblaciones", $_GET['param']);
		if ($json) {
			$jsondata["cities"] = $json;
			echo json_encode($jsondata);
			exit;
		} else {
			$jsondata["cities"] = "error";
			echo json_encode($jsondata);
			exit;
		}
	}

	public function update_user()
	{
		$info_data = json_decode($_POST['total_data'], true);

		$arrArgument = array(
			'nombre' => $info_data['nombre'],
			'avatar' => $info_data['avatar'],
			'provincia' => $info_data['provincia'],
			'ciudad' => $info_data['ciudad'],
			'id_user' => $info_data['id_user']

		);
		$result = loadModel(MODEL_LOGIN, 'login_model', 'update_user', $arrArgument);
		echo json_encode($result);
	}

	public function search_reserves()
	{
		$data = $_GET['param'];

		$result = loadModel(MODEL_LOGIN, 'login_model', 'search_reserves', $data);
		echo json_encode($result);
	}




}
