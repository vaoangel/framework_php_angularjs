<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class profileDAO {
    static $_instance;

    

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function update_profile_DAO($db, $arrArgument) {
        $profname = $arrArgument['nombre'];
        $profemail = $arrArgument['email'];
  
        $pais = $arrArgument['pais'];
        $provincia = $arrArgument['provincia'];
        $ciudad = $arrArgument['ciudad'];
        
        $profpic = $arrArgument['avatar'];

       
        $nameSession = $arrArgument['usuario'];
    
        
        $sql = "UPDATE user set email = '$profemail', pais = '$pais' ,avatar = '$profpic', provincia = '$provincia',id_user = '$profname' , ciudad = '$ciudad' where id_user like '$nameSession'";
      
        return $db->ejecutar($sql);
    }

    public function update_activate_DAO($db,$arrArgument){
      $profname = $arrArgument['nombre'];
      $nameSession = $arrArgument['usuario'];
      $sql = "UPDATE activate set id_user ='$profname' where id_user like '$nameSession'";
      return $db->ejecutar($sql);
    }
    public function update_token_DAO($db,$arrArgument){
      $profname = $arrArgument['nombre'];
      $nameSession = $arrArgument['usuario'];
      $sql = "UPDATE token set id_user ='$profname' where id_user like '$nameSession'";
      return $db->ejecutar($sql);
    }
    public function update_tipo_DAO($db,$arrArgument){
      $profname = $arrArgument['nombre'];
      $nameSession = $arrArgument['usuario'];
      $sql = "UPDATE tipo set id_user ='$profname' where id_user like '$nameSession'";
      return $db->ejecutar($sql);
    }

    

    public function list_purchases_DAO($db, $arrArgument){

      $name = $arrArgument['nombre'];

      $sql = "SELECT * from products where propietario like '$name'";
     // echo $sql;
    $eje = $db->ejecutar($sql);
   //debugPHP($eje);
      return $db->listar($eje);

    }

    public function obtain_countries_DAO($url){
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }

    public function obtain_provinces_DAO(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/1DAW/teamplatev2/resources/provinciasypoblaciones.xml');
          $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
          }
              return $json;

    }

    public function obtain_cities_DAO($arrArgument){
          $json = array();
          $tmp = array();

          $filter = (string)$arrArgument;
          $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/1DAW/teamplatev2/resources/provinciasypoblaciones.xml');
          $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

          for ($i=0; $i<count($result[0]); $i++) {
              $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
              );
              array_push($json, $tmp);
          }
          return $json;
    }
}//End productDAO
