<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;


require(MODEL_PATH . "Db.class.singleton.php");
require(SITE_ROOT . "module/profile/model/DAO/profile_dao.class.singleton.php");

class profile_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = profileDAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function update_profile_BLL($arrArgument){
      return $this->dao->update_profile_DAO($this->db, $arrArgument);
    }

    public function list_purchases_bll($arrArgument){

      return $this->dao->list_purchases_DAO($this->db, $arrArgument);

    }

    public function obtain_countries_BLL($url){
      return $this->dao->obtain_countries_DAO($url);
    }

    public function obtain_provinces_BLL(){
      return $this->dao->obtain_provinces_DAO();
    }

    public function obtain_cities_BLL($arrArgument){
      return $this->dao->obtain_cities_DAO($arrArgument);
    }
    public function update_activate_BLL($arrArgument){
      return $this->dao->update_activate_DAO($arrArgument);

    }
    public function update_token_BLL($arrArgument){
      return $this->dao-> update_token_DAO($arrArgument);
    }
    public function update_tipo_BLL($arrArgument){
      return $this->dao-> update_tipo_DAO($arrArgument);
    }
}
