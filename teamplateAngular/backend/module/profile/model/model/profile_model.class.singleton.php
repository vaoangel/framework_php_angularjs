<?php
//echo json_encode("products model class");
//exit;

require(SITE_ROOT . "module/profile/model/BLL/profile_bll.class.singleton.php");

class profile_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = profile_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function update_profile($arrArgument) {
        return $this->bll->update_profile_BLL($arrArgument);
    }

    public function list_purchases($arrArgument) {
        return $this->bll->list_purchases_bll($arrArgument);
    }

    public function obtain_countries($url){
        return $this->bll->obtain_countries_BLL($url);
    }

    public function obtain_provinces(){
        return $this->bll->obtain_provinces_BLL();
    }

    public Function obtain_cities($arrArgument){
        return $this->bll->obtain_cities_BLL($arrArgument);
    }

    public function update_activate($arrArgument){
        return $this->bll->update_activate_BLL($arrArgument);

    }

    public function update_token($arrArgument){
        return $this->bll->update_token_BLL($arrArgument);

    }

    public function update_tipo($arrArgument){
        return $this->bll->update_tipo_BLL($arrArgument);

    }

}
