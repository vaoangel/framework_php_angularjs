<?php

//site root

$path = $_SERVER['DOCUMENT_ROOT'] . '/1DAW/teamplateAngular/backend/';
define('SITE_ROOT', $path);
//SITE_PATH
define('SITE_PATH', 'https://' . $_SERVER['HTTP_HOST'] . '/1DAW/teamplateAngular/');
//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
//modules
define('MODULES_PATH', SITE_ROOT . 'module/');

//home
define('MODEL_HOME', SITE_ROOT . 'module/home/model/model/');
define('CSS_HOME', SITE_PATH . 'module/home/view/css/');
define('JS_HOME', SITE_PATH . 'module/home/view/js/');
///opt/lampp/htdocs/1DAW/teamplatev2/module/contact/view/js/contact.js
///opt/lampp/htdocs/1DAW/teamplatev2/module/contact/view/js/contact.js

//contact
define('JS_CONTACT_LIB',SITE_PATH .'module/contact/view/lib/');
define('CONTACT_JS_PATH',SITE_PATH .'module/contact/view/js/');
define('CONTACT_CSS_PATH',SITE_PATH .'module/contact/view/css/');
define('CONTACT_IMG_PATH', SITE_PATH . 'module/contact/view/img/'); 
///opt/lampp/htdocs/1DAW/teamplatev2/module/contact/view/js/contact.js

//crud
define('MODEL_CRUD', SITE_ROOT . 'module/crud/model/model/');

//login
define('MODEL_LOGIN', SITE_ROOT . 'module/login/model/model/');
define('CSS_LOGIN', SITE_PATH . 'module/login/view/css/');
define('JS_LOGIN', SITE_PATH . 'module/login/view/js/');
define('UTILS_LOGIN', SITE_ROOT. 'module/login/utils/');
//ubication
define('JS_VIEW_UBICATION', SITE_PATH . 'module/ubication/view/js/');
define('MODEL_UBICATION', SITE_ROOT . 'module/ubication/model/model/');
//profile
define('MODEL_PROFILE', SITE_ROOT . 'module/profile/model/model/');
define('JS_PROFILE', SITE_PATH . 'module/profile/view/js/');
define('UTILS_PROFILE', SITE_PATH . 'module/profile/utils/');
//utils
define('UTILS', SITE_ROOT . 'utils/');
define('MEDIA_ROOT', SITE_ROOT . 'media/');

define('MEDIA_PATH', SITE_PATH . 'backend/media/');

//model
define('MODEL_PATH', SITE_ROOT . 'model/');

//amigables
define('URL_AMIGABLES', TRUE);
//RESOURCES
define('RESOURCES', SITE_ROOT . 'resources/');
//css
define('CSS_PATH', SITE_PATH . 'view/css/');

//JS
define('JS_PATH', SITE_PATH . 'view/plugins/');
define('JS_MODEL', SITE_PATH . 'model/');