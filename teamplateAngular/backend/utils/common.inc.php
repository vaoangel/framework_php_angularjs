<?php
function loadModel($model_path, $model_name, $function, $arrArgument = '') {
    $model = $model_path . $model_name . '.class.singleton.php';
    //echo $model;
 
    if (file_exists($model)) {
        include_once($model);

        $modelClass = $model_name;
        if (!method_exists($modelClass, $function)) {
            throw new Exception();
        }

        $obj = $modelClass::getInstance();
        if (isset($arrArgument)) {
            return call_user_func(array($obj, $function),$arrArgument);//cambiar a array como en router
        }
    } else {
        throw new Exception();
    }
}

function loadView($rutaVista = '', $templateName = '', $arrPassValue = '') {
    $view_path = $rutaVista . $templateName;
    $arrData = '';

    if (file_exists($view_path)) {
        if (isset($arrPassValue))
            $arrData = $arrPassValue;
        include_once($view_path);
    } else {
        //millora per a no utilitzar  ob_start() per evitar dublicació de headers
        $error = filter_num_int($rutaVista);
        if($error['resultado']){
            $rutaVista = $error['datos'];
        }else{
            $rutaVista = http_response_code();
        }
        
        $log = log::getInstance();
        $log->add_log_general("error loadView general", $_GET['module'], "response " . $rutaVista); //$text, $controller, $function
        $log->add_log_user("error loadView general", "", $_GET['module'], "response " . $rutaVista); //$msg, $username = "", $controller, $function

        $result = response_code($rutaVista);
        $arrData = $result;
        require_once VIEW_PATH_INC_ERROR . "error.php";
    }
}
function validate_profile($value){

    $error = array();
    $valid = true;

    //debugPHP($value,"valprofile");

    $filter = array(
        'nombre' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z\s]{2,30}$/')
        )
        
    );

    $result = filter_var_array($value,$filter);

    //debugPHP($result);

    $result['email'] = $value['email'];
    $result['pais'] = $value['pais'];
    $result['provincia'] = $value['provincia'];
    $result['ciudad'] = $value['ciudad'];
    


    if($result['nombre']==='Input  name'){
        $error['nombre']="Name must be 2 to 30 letters";
        $valid = false;
}
    if ($result['pais']==='Select a country'){
        $error['pais']="You need to choose a country";
        $valid = false;
    }

if ($result['provincia']==='Select a province'){
        $error['provincia']="You need to choose a province";
        $valid = false;
    }

if ($result['ciudad']==='Select a city'){
        $error['ciudad']="You need to choose a city";
        $valid = false;
    }

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}
