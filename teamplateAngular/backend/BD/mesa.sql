-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 04-06-2019 a les 21:51:30
-- Versió del servidor: 10.1.37-MariaDB
-- Versió de PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `mesa`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `activate`
--

CREATE TABLE `activate` (
  `id_user` varchar(500) NOT NULL,
  `activate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `activate`
--

INSERT INTO `activate` (`id_user`, `activate`) VALUES
('eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.eyJpYXQiOjE1NTk2NzczMjYsImV4cCI6MTU1OTY3NzM1NiwiaWQiOiJ2YW9hbmdlbCIsInR5cGUiOiIifQ.dWkSu2WwulC7FeXYWU6RLcxLHoHfbFALNKtruNaOF3w', 1),
('w89UnRjTVsG5KuUzTIWrT_1w6Sizpo6R', 1);

-- --------------------------------------------------------

--
-- Estructura de la taula `products`
--

CREATE TABLE `products` (
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `propietario` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `products`
--

INSERT INTO `products` (`nombre`, `descripcion`, `cantidad`, `precio`, `propietario`) VALUES
('miranda', 'asddasdasasddas', 34, 12, 'update'),
('miranda2', 'asddasdasasddas2', 342, 122, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update'),
('Miranda23', 'descripcionaskdasdasda', 55, 22, 'update');

-- --------------------------------------------------------

--
-- Estructura de la taula `reserva`
--

CREATE TABLE `reserva` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `surname` varchar(60) DEFAULT NULL,
  `numpeople` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `menu` varchar(200) DEFAULT NULL,
  `reserve_date` varchar(12) DEFAULT NULL,
  `alergens` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `reserva`
--

INSERT INTO `reserva` (`id`, `name`, `surname`, `numpeople`, `email`, `menu`, `reserve_date`, `alergens`) VALUES
(127, 'Angel VaÃ±o', 'Vaads', 2121, 'vaoangel@gmail.com', 'menu2', '08/01/2019', 'celiaco:carne:verduras:'),
(128, 'Angel ', 'Vanio', 2121, 'vaoangel@gmail.com', 'menu2', '14/11/2019', 'celiaco:carne:');

-- --------------------------------------------------------

--
-- Estructura de la taula `reserves`
--

CREATE TABLE `reserves` (
  `propietario` varchar(45) NOT NULL,
  `a_nombre_de` varchar(45) NOT NULL,
  `restaurante` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `fecha` varchar(45) DEFAULT NULL,
  `hora` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `reserves`
--

INSERT INTO `reserves` (`propietario`, `a_nombre_de`, `restaurante`, `email`, `fecha`, `hora`) VALUES
('$propietario', '$nombre_contacto', '$restaurante', '$email', '$fecha', '$hora'),
('$propietario', '$nombre_contacto', '$restaurante', '$email', '$fecha', '$hora'),
('$propietario', '$nombre_contacto', '$restaurante', '$email', '$fecha', '$hora'),
('vaoangel', 'vaoangel', 'Miranda', 'vaoangel@gmail.com', '', ''),
('vaoangel', 'asdasdasd', 'Miranda', 'vaoangel@gmail.com', '2019-05-18T22:00:00.000Z', '14:14'),
('vaoangel', 'vaoangel', 'Miranda', 'vaoangel@gmail.com', '2019-05-18T22:00:00.000Z', '15:03'),
('vaoangel', 'asdasdasd', 'Miranda', 'vaoangel@gmail.com', '2019-05-05T22:00:00.000Z', '15:03'),
('vaoangel', 'assdaasd', 'Casa ximo', 'vaoangel@gmail.com', '2019-05-19T22:00:00.000Z', '15:03');

-- --------------------------------------------------------

--
-- Estructura de la taula `restaurantes`
--

CREATE TABLE `restaurantes` (
  `nombre` text NOT NULL,
  `provincia` text NOT NULL,
  `ciudad` text NOT NULL,
  `calidad` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `lat` varchar(45) NOT NULL,
  `longi` varchar(45) NOT NULL,
  `imagen` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `restaurantes`
--

INSERT INTO `restaurantes` (`nombre`, `provincia`, `ciudad`, `calidad`, `descripcion`, `lat`, `longi`, `imagen`) VALUES
('Miranda', 'Valencia', 'Ontinyent', 444, 'Abierto de 4 a 8', '23123123123', '12312312312312', 'https://localhost/1DAW/teamplateAngular/backend/media/22524-flowers.png'),
('Miranda', 'Valencia', 'Ontinyent', 444, 'Abierto de 4 a 8', '23123123123', '12312312312312', 'https://localhost/1DAW/teamplateAngular/backend/media/22524-flowers.png'),
('Casa ximo', 'Valencia', 'Bocairent', 33, 'Abierto de 4 a 8', '23423423', '23423423', 'https://localhost/1DAW/teamplateAngular/backend/media/22524-flowers.png'),
('Casa ximo', 'Valencia', 'Bocairent', 33, 'Abierto de 4 a 8', '23423423', '23423423', 'https://localhost/1DAW/teamplateAngular/backend/media/22524-flowers.png'),
('Villa', 'Alicante', 'Muro De Alcoy', 44, 'abierto de 4 a 8', '-343434', '-34343434', 'https://localhost/1DAW/teamplateAngular/backend/media/13005-flowers.png'),
('Sifo', 'Valencia', 'Bocairent', 33, 'Abierto de 4 a 8', '12123123', '123123123', 'https://localhost/1DAW/teamplateAngular/backend/media/22524-flowers.png');

-- --------------------------------------------------------

--
-- Estructura de la taula `tipo`
--

CREATE TABLE `tipo` (
  `id_user` varchar(500) NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `tipo`
--

INSERT INTO `tipo` (`id_user`, `tipo`) VALUES
('eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.eyJpYXQiOjE1NTk2NzczMjYsImV4cCI6MTU1OTY3NzM1NiwiaWQiOiJ2YW9hbmdlbCIsInR5cGUiOiIifQ.dWkSu2WwulC7FeXYWU6RLcxLHoHfbFALNKtruNaOF3w', 'admin'),
('w89UnRjTVsG5KuUzTIWrT_1w6Sizpo6R', 'admin');

-- --------------------------------------------------------

--
-- Estructura de la taula `token`
--

CREATE TABLE `token` (
  `id_user` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `token`
--

INSERT INTO `token` (`id_user`, `token`) VALUES
('vaoangel', 'eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.eyJpYXQiOjE1NTk2NzczMjYsImV4cCI6MTU1OTY3NzM1NiwiaWQiOiJ2YW9hbmdlbCIsInR5cGUiOiIifQ.dWkSu2WwulC7FeXYWU6RLcxLHoHfbFALNKtruNaOF3w'),
('vaoangel_social', 'w89UnRjTVsG5KuUzTIWrT_1w6Sizpo6R');

-- --------------------------------------------------------

--
-- Estructura de la taula `user`
--

CREATE TABLE `user` (
  `id_user` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` text NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `provincia` varchar(45) NOT NULL,
  `ciudad` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `user`
--

INSERT INTO `user` (`id_user`, `nombre`, `email`, `password`, `avatar`, `provincia`, `ciudad`) VALUES
('vaoangel', 'vaoangel', 'vaoangel@gmail.com', '$2y$12$tnmGtfmfTt5F8lpUbd2/zuCYnqtIyph/.ptVGE0rYZs6SjCGnRvnm', 'https://localhost/1DAW/teamplateAngular/backend/media/25172-flowers.png', 'Not set', 'Not set'),
('vaoangel_social', 'vaoangel_social', 'vaoangel@gmail.com', 'Social_login', 'https://lh3.googleusercontent.com/-nmKzC5caI3M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rd0MgpcPWYfnUCg1z6B8G3-q', 'not_set', 'not_set');

-- --------------------------------------------------------

--
-- Estructura de la taula `usuari`
--

CREATE TABLE `usuari` (
  `nombre` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `tipo` text NOT NULL,
  `pais` text NOT NULL,
  `provincia` text NOT NULL,
  `ciudad` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `usuari`
--

INSERT INTO `usuari` (`nombre`, `email`, `password`, `avatar`, `tipo`, `pais`, `provincia`, `ciudad`) VALUES
('pacopepe', 'pacopepe@gmail.com', '123456', 'adsssssssssssssssssssssssssssssssssssssssssssssssssssssss', 'admin', '', '', ''),
('admin1', 'admin1@gmail.com', '$2y$12$S8rf.TkRpAr3tNtRp.zNBOjgUx3ydhT7Qt1Hhez8RLA1wuv84bovi', 'https://adorable.io/avatars/128/admin1@gmail.com', '', '', '', ''),
('admin1', 'admin1@gmail.com', '$2y$12$65dJFTGor.kvSIKKI3TpY.1mXiVj8.DGAqIZ2CITONFNosa9whtuC', 'https://adorable.io/avatars/128/admin1@gmail.com', '', '', '', ''),
('admin2', 'admin2@gmail.com', '$2y$12$l6QUuKzGQA0ICQFTdBfktegLMLmJwyPxGSflvAP4hQi3FBVZC0Hce', 'https://adorable.io/avatars/128/admin2@gmail.com', '', '', '', ''),
('admin2', 'admin2@gmail.com', '$2y$12$JMyw6zwpwqV138E1COn8peAFAt.2W6IsnqRiTd1dr.M7.xJ2XadK6', 'https://adorable.io/avatars/128/admin2@gmail.com', '', '', '', ''),
('paco', 'paco@gmail.com', '$2y$12$Duk/TE32w0joZmfMyxpxNOM8gMl.29Qg5ljyobsKhrYtTtL5enIoC', 'https://adorable.io/avatars/128/paco@gmail.com', '', '', '', ''),
('paco', 'paco@gmail.com', '$2y$12$YvqIh/sg7snW9fwHwsSnouz3t9RYSkoYD0BwUowCJzbZMrLDlxqM2', 'https://adorable.io/avatars/128/paco@gmail.com', '', '', '', ''),
('admin12', 'admin12@gmail.com', '$2y$12$j/J/XvR5XQg0zFjEFTPl3eGJ4ym0AAgQIMJ3mWhtEBA.BROMy0LBC', 'https://adorable.io/avatars/128/admin12@gmail.com', '', '', '', ''),
('admin12', 'admin12@gmail.com', '$2y$12$JEp85/wHtttZeCKigXSH1uyFhgnQ4qDjDmGDom7BozpDVuuADeRXq', 'https://adorable.io/avatars/128/admin12@gmail.com', '', '', '', ''),
('update', 'vaoangel@gmail.com', '$2y$12$yxMLIXulb57h.aK4LPd2d.lhF91rgXrO2V8Koer.Qgc6v5GL1N6p.', '/1DAW/teamplate/media/6048-flowers.png', 'admin', 'RW', 'default_province', 'default_city'),
('Angel', 'vaoangel@gmail.com', '$2y$12$uq4kDFGQin0Y6U.LJ4xU4OrcV12BZC0sRhcSt8ySgMndDabcltVd2', '1DAW/teamplate/media/default-avatar.png', 'admin', 'RW', 'default_province', 'default_city'),
('asdsadas', 'adsadsdasdas', 'asddsadsaads', 'asddsasda', '', '', '', ''),
('asdsadas', 'adsadsdasdas', 'asddsadsaads', 'asddsasda', '', '', '', ''),
('asdsadgggggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadgggggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadgggggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggssadasasddasasdasdadsdasdasgggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggooooooooooooooooooooooooooogggggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', ''),
('asdsadggggggggfgffffffffffffffffffffffffffffffffffffffffffffgggggas', 'adsadsdasdasgggggggggggg', 'asddgggggggggsadsaads', 'ggggggggggggsddsasda', '', '', '', '');

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `activate`
--
ALTER TABLE `activate`
  ADD PRIMARY KEY (`id_user`);

--
-- Índexs per a la taula `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`);

--
-- Índexs per a la taula `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_user`);

--
-- Índexs per a la taula `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id_user`);

--
-- Índexs per a la taula `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
