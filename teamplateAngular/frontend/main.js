var restaurants = angular.module('restaurants', ['ngRoute', 'toastr', 'ui.bootstrap', 'ngMaterial']);


restaurants.config(function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'frontend/module/home/view/home.html',
            controller: 'mainController',
            resolve: {
                products: function(services) {

                    return services.get('home', 'get_restaurants');

                }

            }
        })
        .when('/home', {
            templateUrl: 'frontend/module/home/view/home.html',
            controller: 'mainController',
            resolve: {
                products: function(services) {

                    return services.get('home', 'get_restaurants');

                }

            }
        })
        .when("/home/:id", {
            templateUrl: "frontend/module/home/view/restaurant.view.html",
            controller: "detailsCtrl",
            resolve: {
                data: function(services, $route) {

                    return services.get('home', 'details_products', $route.current.params.id);
                },

                rating: function(services, $route) {
                    return services.get('home', 'rate_prods', '');

                }
            }
        })

    .when("/shop", {
        templateUrl: "frontend/module/shop/view/shop.html",
        controller: "shopCtrl",
        resolve: {
            data: function(services) {

                return services.get('home', 'get_restaurants');

            }

        }
    })

    .when("/shop/:id", {
        templateUrl: "frontend/module/shop/view/reserva.html",
        controller: "reservaCtrl",
        resolve: {
            id: function($route) {
                return $route.current.params.id;
            }
        }
    })

    .when("/login", {
        templateUrl: "frontend/module/login/view/forms.html",
        controller: "loginCtrl",
        // resolve: {
        //     data: function (services, $route) {
        //         console.log($route);
        //         //return services.get('home', 'details_products', $route.current.params.id);
        //     }
        // }
    })

    .when("/login/recover", {
            templateUrl: "frontend/module/login/view/forms.html",
            controller: "recoverCtrl",
            resolve: {
                data: function(services, $route) {
                    console.log($route);
                }
            }

        })
        .when("/login/profile", {
            templateUrl: "frontend/module/login/view/profile.view.html",
            controller: "profileCtrl",
            resolve: {
                infoUser: function(services, localstorageService) {
                    //console.log(localstorageService.get_id_user());
                    return services.get('login', 'search_user', localstorageService.get_id_user());
                },
                data: function(services, localstorageService) {
                    return services.get('login', 'search_reserves', localstorageService.get_id_user());
                },
                provinces: function(services) {
                    return services.get('login', 'load_provincias_login');
                },
                restaurants: function(services) {

                    return services.get('home', 'get_restaurants');

                }

            }
        })
        .when("/login/recover/enter_form/:token", {
            templateUrl: "frontend/module/login/view/recover.html",
            controller: "recover_formCtrl",
            resolve: {
                data: function(services, $route) {
                    console.log($route);
                    return $route.current.params.token;
                }
            }

        })

    .when("/login:logout", {
        templateUrl: 'frontend/module/home/view/home.html',
        controller: "logoutCtrl",
    })

    .when("/login/active_user/:token", {

        templateUrl: "frontend/module/login/view/forms.html",
        controller: "loginCtrl",

        resolve: {
            activate: function(services, $route) {
                console.log($route.current.params.token);
                return services.put('home', 'active_user', { 'token': JSON.stringify({ 'token': $route.current.params.token }) })
                    .then(function(response) {
                        console.log(response);
                        location.href = '#/';
                    });

            }
        }
    })

    .when("/crud", {
            templateUrl: "frontend/module/crud/view/options.html",
            controller: "crudCtrl",
            resolve: {
                provinces: function(services) {
                    return services.get('login', 'load_provincias_login');
                },
                products: function(services) {

                    return services.get('home', 'get_restaurants2');

                }

            }
        })
        .when('/contacto', {
            templateUrl: 'frontend/module/contact/view/contact.html',
            controller: 'contactController'
        })

    .otherwise({
        redirectTo: '/'
    });
});
restaurants.controller('mainController', function($scope) {
    $scope.message = 'Hola, Mundo!';
});



restaurants.controller('contactController', function($scope) {});
restaurants.controller('menuCtrl', function($scope) {
    loginV = true;
});