restaurants.controller('crudCtrl', function($scope, provinces, products, load_pais_prov_poblac, $route, toastr, loginService, localstorageService, services, $timeout) {
	// console.log(products.length);
	//     var range = [];
	//     for(var i=0;i<=products.length;i++) {

	//       range.push(products[i].nombre);
	//     }
	//     console.log(range);


	//agafa dades de localstorage per a comprovar si estas loguejat i eres de tipo admin
	testing = localstorageService.getToken();
	id_user = localstorageService.get_id_user();

	if (!testing) {
		location.href = "#/"; //si no estàs loguejat va al home

	} else {
		data = { 'oldtoken': testing, 'id_user': id_user };
		//console.log(data);
		services.post('home', 'token_match', { 'total_data': JSON.stringify(data) }).then(function(response) {
			//  console.log(response);

			if ((response[0].token != testing) || (response[0].tipo != 'admin')) {
				location.href = "#/"; //Si no estas loguejat o no eres de tipo admin va al home
			} else {
				//    console.log("coincide");
			}


		});
	}




	social = localstorageService.getsocial();
	//console.log(social);

	if (social != 'si') {

		//renova el token del usuari cada X temps
		setInterval(function() {
			testing = localstorageService.get_id_user();

			if (testing) {
				oldtoken = localstorageService.getToken();
				id_user = localstorageService.get_id_user();
				data = { 'oldtoken': oldtoken, 'id_user': id_user };
				services.post('home', 'timeout_token', { 'total_data': JSON.stringify(data) }).then(function(response) {
					console.log(response);
					console.log(data);
					if ((response.result1 == false) || (response.result2 == false) || (response.result3 == false)) {
						alert("ErrorToken");
					} else {
						localstorageService.setUsers(response.newtoken);
						console.log("works");
					}


					// pepe = localstorageService.getToken();
					// console.log(pepe);
				})
			} else {
				console.log("not loged");
			}



		}, 12000000);
	}

	$scope.restaurante = products;

	//console.log($scope.restaurante);
	$scope.Cprov = true;
	$scope.Uprov = false;
	$scope.Dprov = false;
	$scope.showCprof = function() {
		$scope.Cprov = true;
		$scope.Uprov = false;
		$scope.Dprov = false;
	}
	$scope.showEprof = function() {
		$scope.Dprov = false;
		$scope.Uprov = true;
		$scope.Cprov = false;
	}
	$scope.showDprof = function() {
		$scope.Cprov = false;
		$scope.Uprov = false;
		$scope.Dprov = true;
	}
	$scope.profV = true;
	$scope.dropzoneConfig = {
		'options': {
			'url': 'backend/index.php?module=login&function=upload_avatar',
			addRemoveLinks: true,
			maxFileSize: 1000,
			dictResponseError: "Ha ocurrido un error en el server",
			acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
		},
		'eventHandlers': {
			'sending': function(file, formData, xhr) {},
			'success': function(file, response) {
				console.log(response);
				response = JSON.parse(response);
				//console.log(response);
				if (response.resultado) {
					$(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
					$('.msg').animate({ 'right': '300px' }, 300);

					//console.log(response.datos);
					$scope.avatar = response.datos;

					// var user = {usuario: $scope.userInfo.nombre, avatar: response.datos, nombre: $scope.userInfo.nombre};
					// cookiesService.SetCredentials(user);

					loginService.login();
				} else {
					$(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
					$('.msg').animate({ 'right': '300px' }, 300);
				}
			},
			'removedfile': function(file, serverFileName) {
				if (file.xhr.response) {
					$('.msg').text('').removeClass('msg_ok');
					if ((response[0].token != testing) || (response[0].tipo != 'admin')) {
						location.href = "#/"
					} else {
						console.log("coincide");
					}
					$('.msg').text('').removeClass('msg_error');
					var data = jQuery.parseJSON(file.xhr.response);
					services.post("user", "delete_avatar", JSON.stringify({ 'filename': data }));
				}
			}
		}
	};

	$scope.provinces = provinces.provincias;
	//console.log($scope.provinces);
	$scope.changeProv = function() {
		if ($scope.crud_form.selectProvincia.id) {
			services.get('login', 'load_poblaciones_login', $scope.crud_form.selectProvincia.id).then(function(response) {
				//          console.log($scope.profile_form.selectProvincia.id);
				$scope.city = response.cities;
			});
		}



	}
	$scope.changeProvU = function() {
		if ($scope.crud_form_u.selectProvincia.id) {
			services.get('login', 'load_poblaciones_login', $scope.crud_form_u.selectProvincia.id).then(function(response) {
				//          console.log($scope.profile_form.selectProvincia.id);
				$scope.city = response.cities;
			});
		}



	}


	$scope.submit = function() {
		user = localstorageService.get_id_user();
		var data = { 'nombre': $scope.crud_form.inputName.$viewValue, 'provincia': $scope.crud_form.selectProvincia.nombre, 'poblacion': $scope.crud_form.selectCity.poblacion, 'imagen': $scope.avatar, 'lat': $scope.crud_form.lat, 'long': $scope.crud_form.long, 'calidad': $scope.crud_form.cal, 'descr': $scope.crud_form.Descr, 'user': user };
		services.post('crud', 'insert_product', { 'total_data': JSON.stringify(data) }).then(function(response) {


			console.log(response);

			if (response === 'true') {
				toastr.success('Datos insertados correctamente', 'Perfecto', {
					closeButton: true
				});
				location.href = '#/';
			}


		});
	}

	$scope.submitU = function() {
		var data = { 'restaurant': $scope.crud_form_u.selectedRestaurant, 'provincia': $scope.crud_form_u.selectProvincia.nombre, 'poblacion': $scope.crud_form_u.selectCity.poblacion, 'imagen': $scope.avatar, 'lat': $scope.crud_form_u.lat, 'long': $scope.crud_form_u.long, 'calidad': $scope.crud_form_u.cal, 'descr': $scope.crud_form_u.Descr };

		console.log(data);
		services.post('crud', 'update_product', { 'total_data': JSON.stringify(data) }).then(function(response) {


			console.log(response);

			if (response === 'true') {
				toastr.success('Datos actualizados correctamente', 'Perfecto', {
					closeButton: true
				});
				location.href = '#/';
			} else {
				toastr.error('Ha ocurrido un problema con el insert', 'Error', {
					closeButton: true
				});
			}


		});
	}

	$scope.submitD = function() {
		var data = { 'id': $scope.crud_form_d.selectedRestaurant };
		services.post('crud', 'delete_product', { 'total_data': JSON.stringify(data) }).then(function(response) {


			console.log(response);

			if (response === 'true') {
				toastr.success('Restaurante eliminado correctamente', 'Perfecto', {
					closeButton: true
				});
				location.href = '#/';
			}


		});
	}
})