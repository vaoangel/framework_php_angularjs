restaurants.controller('contactController', function($scope, services, $timeout) {

    $scope.contact = {
        inputName: "",
        inputEmail: "",
        inputSubject: "",
        inputMessage: ""
    };
    //Envia la informació del formulari de contacte al correu
    $scope.SubmitContact = function() {
        var data = {
            "inputName": $scope.contact.inputName,
            "inputEmail": $scope.contact.inputEmail,
            "inputSubject": $scope.contact.inputSubject,
            "inputMessage": $scope.contact.inputMessage,
            "token": 'contact_form'
        };
        var contact_form = JSON.stringify(data);
        //console.log(contact_form);

        services.post('contact', 'process_contact', contact_form).then(function(response) {

            //   console.log(response);


            $scope.bannerText = response;
            //$scope.bannerClass = 'alertbanner aletbanner' + type;
            $scope.bannerV = true;

            $timeout(function() {
                $scope.bannerV = false;
                $scope.bannerText = "";
            }, 5000);

        })
    }
});