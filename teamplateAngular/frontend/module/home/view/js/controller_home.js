restaurants.controller('mainController', function($scope, services, $route, $timeout, toastr, localstorageService, loginService) {

	social = localstorageService.getsocial();
	console.log(social);
// detecta si el usuari es de tipo social i si no ho es renova el token periodicament
	if (social != 'si') {
		setInterval(function() {
			testing = localstorageService.get_id_user();

			if (testing) {
				oldtoken = localstorageService.getToken();
				id_user = localstorageService.get_id_user();
				data = { 'oldtoken': oldtoken, 'id_user': id_user };
				services.post('home', 'timeout_token', { 'total_data': JSON.stringify(data) }).then(function(response) {
					// console.log(response);
					// console.log(data);
					if ((response.result1 == false) || (response.result2 == false) || (response.result3 == false)) {
						alert("ErrorToken");
					} else {
						localstorageService.setUsers(response.newtoken);
						console.log("works");
					}


					// pepe = localstorageService.getToken();
					// console.log(pepe);
				})
			} else {
				console.log("not loged");
			}



		}, 12000000);
	}



	//console.log(services.products);
	//console.log(restaurants);
	$scope.currentPage = 1;
	$scope.filteredRestaurants;
	loginService.login();
	//location.reload();
	$scope.numPerPage = 6;
	$scope.maxSize = 5;
	$scope.currentPage = 1;

	// var test1 = localstorageService.get_id_user();
	// console.log(test1);

	//pinta els productes
	services.get('home', 'get_restaurants', '').then(function(response) {

		//console.log(response);

		$scope.filteredRestaurants = response;
		$scope.filteredPaint = $scope.filteredRestaurants.slice(0, 3);

		//console.log($scope.filteredRestaurants.length);
		$scope.bannerText = response;
		//$scope.bannerClass = 'alertbanner aletbanner' + type;
		$scope.bannerV = true;

		$timeout(function() {
			$scope.bannerV = false;
			$scope.bannerText = "";
		}, 5000);

	});


//Pagination 
	$scope.pageChanged = function() {
		var startPos = ($scope.currentPage - 1) * 3;
		$scope.filteredPaint = $scope.filteredRestaurants.slice(startPos, startPos + 3);
		//console.log($scope.currentPage);
	};



	$scope.myInterval = 5000;
	$scope.noWrapSlides = false;
	$scope.active = 0;
	var slides = $scope.slides = [];
	var currIndex = 0;

	$scope.addSlide = function() {
		var newWidth = 600 + slides.length + 1;
		// console.log(newWidth);
		slides.push({
			image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsenlXy0QP_Negqd7fa0iiXiUk5sVC4RKo-pG38jVW3sMtVyLB',
			// text: ['Nice image','Awesome photograph','That is so cool','I love that'][slides.length % 4],
			id: currIndex++
		});
	};

	$scope.randomize = function() {
		var indexes = generateIndexesArray();
		assignNewIndexesToSlides(indexes);
	};

	for (var i = 0; i < 4; i++) {
		$scope.addSlide();
	}

	// Randomize logic below

	function assignNewIndexesToSlides(indexes) {
		for (var i = 0, l = slides.length; i < l; i++) {
			slides[i].id = indexes.pop();
		}
	}



	// http://stackoverflow.com/questions/962802#962890
	function shuffle(array) {
		var tmp, current, top = array.length;

		if (top) {
			while (--top) {
				current = Math.floor(Math.random() * (top + 1));
				tmp = array[current];
				array[current] = array[top];
				array[top] = tmp;
			}
		}

		return array;
	}

});
//Products details
restaurants.controller('detailsCtrl', function($scope, $route, rating, localstorageService, $rootScope, events_map, data, services, $timeout, $compile) {
	$scope.data = data[0];
	user = localstorageService.get_id_user();
	$scope.filteredRestaurants = rating;
	$scope.filteredPaint = $scope.filteredRestaurants.slice(0, 3);
	console.log(rating);

	if (!user) {
		$rootScope.rating = false;
	} else {
		$rootScope.rating = true;

	}
	//rating products
	$scope.rateChange = function() {
		//console.log($scope.data.nombre);
		//console.log($scope.rate);

		location.reload();
		if (user) {
			data = { 'rate': $scope.rate, 'user': user, 'restaurant': $scope.data.nombre };
		}
		//console.log(data);
		services.post('home', 'update_rate', { 'total_data': JSON.stringify(data) }).then(function(response) {

			//console.log(response);
		});

	};
	//console.log(data);
	//geolocalització
	$scope.showMap = function() {
		if ($scope.mapholderV) {
			btnShowMap.innerHTML = "Ver resultados en el mapa";
			$scope.mapholderV = false;
		} else {
			events_map.cargarmap($scope.data, $scope, $compile);
			btnShowMap.innerHTML = "Ocultar mapa";
			$scope.mapholderV = true;
		}
	};
})