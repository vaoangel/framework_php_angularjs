var socialinfo;
var webAuth;

function inicialiceWebAuth() {
    webAuth = new auth0.WebAuth({
        domain: 'vaoangel.eu.auth0.com',
        clientID: 'gPjRty1h045MuI0pXHIsLGmk1POru355',
        responseType: 'token id_token',
        redirectUri: 'http://localhost/1DAW/teamplateAngular/login',
        // audience: 'https://' + 'vaoangel.eu.auth0.com' + '/userinfo',
        // responseType: 'token id_token',
        // scope: 'openid profile',
        // leeway: 60
    });

}

// 
function socialLoginPopup(err, authResult) {
    if (authResult && authResult.accessToken && authResult.idToken) {
        //sendSocialLoginUser(authResult);
        // console.log(authResult);
        //location.href = '/login/social/:'+authResult;
        socialinfo = authResult;
    }
}

function socialLogin() {
    webAuth.popup.authorize({}, function() {});
}

function socialLogout() {
    localStorage.removeItem('socialLogin');
    webAuth.logout({
        returnTo: window.location.href,
        client_id: 'gPjRty1h045MuI0pXHIsLGmk1POru355'
    });
}


restaurants.controller('loginCtrl', function($scope, services, $timeout, toastr, localstorageService, loginService, googleService) {
    //comprobar = localstorageService.getTipo();

    // if (comprobar != 'admin'){
    //     location.href = '#/';

    // }

    inicialiceWebAuth();

    $scope.logGoogle = function() {

        webAuth.popup.authorize({}, function() {

            if (!socialinfo.idTokenPayload.email) {
                socialinfo.idTokenPayload.email = socialinfo.idTokenPayload.nickname + "@gmail.com";
            }
            var data = { 'token': socialinfo.accessToken, 'user': socialinfo.idTokenPayload.nickname + "_social", 'email': socialinfo.idTokenPayload.email, 'avatar': socialinfo.idTokenPayload.picture };

            services.post('login', 'social_login', { 'total_data': JSON.stringify(data) }).then(function(response) {
                //console.log(response);
                // console.log(data);
                if ((response.result1 == false) || (response.result2 == false) || (response.result3 == false) || (response.result4 == false)) {

                    console.log("error db");

                } else {
                    toastr.success('login completo', 'Perfecto', {
                        closeButton: true
                    });
                    localstorageService.setUsers(response.token);
                    localstorageService.setID_user(data.user);
                    localstorageService.set_Tipo(response.tipo);
                    localstorageService.setsocial('si');
                    location.href = '#/';






                }

            })
        });

    };

    $scope.submitRegister = function() {
        var data = { 'ruser': $scope.register.inputUser, 'remail': $scope.register.inputEmail, 'rpasswd': $scope.register.inputPassword };
        //console.log(data);

        services.post('login', 'validate_register', { 'total_data': JSON.stringify(data) }).then(function(response) {
            //console.log(response);

            if (response == 'siexiste') {
                alert("El usuario ya existe");
            } else {

                if ((response.result1 == false) || (response.result2 == false) || (response.result3 == false) || (response.result4 == false)) {

                    //console.log("error db");
                    toastr.error('Ha ocurrido un problema con el egistro', 'Error', {
                        closeButton: true
                    });

                } else {
                    //console.log("All true");



                    toastr.success('Revisa tu correo porfavor', 'Perfecto', {
                        closeButton: true
                    });
                    // console.log(response);

                    //location.href = '/login';
                }
            }

        })
    }

    $scope.submitLogin = function() {
        var data = { 'luser': $scope.login.inputUser, 'lpasswd': $scope.login.inputPassword };
        //console.log(data);

        services.post('login', 'validate_login', { 'total_data': JSON.stringify(data) }).then(function(response) {


            //console.log(response);
            if (response == 'noexiste') {
                alert("El usuario introducido no existe");
            } else if (response == 'wrongpass') {
                alert("El la contraseña es incorrecta");
            } else if (response == 'inactivo') {
                alert("Su cuenta no esta activada, porfavor revise su correo");
            } else {
                toastr.success('Login completado correctamente', 'Perfecto', {
                    closeButton: true
                });
                localstorageService.setUsers(response.token);
                localstorageService.setID_user(response.id_user);
                localstorageService.set_Tipo(response.tipo);
                loginService.login();
                location.href = '#/';

            }

        });

    }


    $scope.logOut = function() {
        socialLogout();
    }

})

restaurants.controller('logoutCtrl', function($scope, toastr, $route, toastr, loginService, services, $timeout) {

    loginService.logout();
    $route.reload();

    toastr.success('Sesión cerrada correctamente', 'Perfecto', {
        closeButton: true
    });
    location.href = '#/';
})
restaurants.controller('recoverCtrl', function($scope, $route, toastr, loginService, localstorageService, services, $timeout) {

    //console.log("entra");

    var token = localstorageService.getToken();
    var id_user = localstorageService.get_id_user();
    var data = { 'user': id_user, 'token': token };
    services.post('login', 'recover_passwd', { 'total_data': JSON.stringify(data) }).then(function(response) {

        toastr.success('Revisa tu correo ', 'Perfecto', {
            closeButton: true
        });
        //alert("Porfavor revisa tu correo");
        location.href = '#/';




    });

})
restaurants.controller('recover_formCtrl', function($scope, $route, data, loginService, localstorageService, services, $timeout) {

    var tokenuser = localstorageService.getToken();
    if (data != tokenuser) {
        toastr.error('Error de token porfavor vuelva a iniciar sesión', 'Error', {
            closeButton: true
        });
        // alert("Error de token porfavor vuelva a iniciar sesión");
    } else {
        $scope.submitRecPass = function() {
            //console.log($scope.recpass.inputPassword);
            var data = { 'password': $scope.recpass.inputPassword, 'id_user': "data" };
            services.post('login', 'recover_passwd_in', { 'total_data': JSON.stringify(data) }).then(function(response) {


                //console.log(response);

                if (response === 'true') {
                    toastr.success('Contraseña actualizada correctamente', 'Perfecto', {
                        closeButton: true
                    });
                    location.href = '#/';

                } else {
                    alert("ha ocurrido un error, porfavor vuelva a intentarlo");
                    location.href = '#/';

                }


            });
        }
    }
    //console.log(data);




})
restaurants.controller('profileCtrl', function($scope, infoUser, restaurants, provinces, data, load_pais_prov_poblac, $route, loginService, localstorageService, services, $timeout) {
    $scope.sprofV = true;
    $scope.eprofV = false;
    $scope.cprofileV = false;
    //console.log(restaurants);
    $scope.avatar = infoUser[0].avatar;
    $scope.userInfo = infoUser[0];
    //console.log($scope.userInfo);

    $scope.showSprof = function() {
        $scope.sprofV = true;
        $scope.eprofV = false;
        $scope.cprofileV = false;
    }
    $scope.showEprof = function() {
        $scope.sprofV = false;
        $scope.eprofV = true;
        $scope.cprofileV = false;
    }
    $scope.showCprof = function() {
            $scope.sprofV = false;
            $scope.eprofV = false;
            $scope.cprofileV = true;
        }
        //console.log(provinces.provincias);
    $scope.provinces = provinces.provincias;
    $scope.changeProv = function() {
            if ($scope.profile_form.selectProvincia.id) {
                services.get('login', 'load_poblaciones_login', $scope.profile_form.selectProvincia.id).then(function(response) {
                    //          console.log($scope.profile_form.selectProvincia.id);
                    $scope.city = response.cities;
                });
            }
        }
        // load_pais_prov_poblac.load_pais()
        //     .then(function (response) {
        //         console.log(response);
        //         if (response.success) {
        //             $scope.paises = response.datas;
        //         } else {
        //             $scope.AlertMessage = true;
        //             $scope.userInfo.pais_error = "Error al recuperar la informacion de paises";
        //             $timeout(function () {
        //                 $scope.userInfo.pais_error = "";
        //                 $scope.AlertMessage = false;
        //             }, 2000);
        //         }
        //     });
        // $scope.resetPais = function () {
        //   console.log("entra")
        //         load_pais_prov_poblac.loadProvincia()
        //             .then(function (response) {
        //                 console.log(response);
        //                 if (response.success) {
        //                     $scope.provincias = response.datas;
        //                 } else {
        //                     $scope.AlertMessage = true;
        //                     $scope.user.prov_error = "Error al recuperar la informacion de provincias";
        //                     $timeout(function () {
        //                         $scope.user.prov_error = "";
        //                         $scope.AlertMessage = false;
        //                     }, 2000);
        //                 }
        //             });
        //         $scope.poblaciones = null;
        //      /*else { //en ng-disabled
        //         $scope.provincias = null;
        //         $scope.poblaciones = null;
        //     }*/
        // };
        // $scope.resetValues = function () {
        //     var datos = { idPoblac: $scope.user.provincia.id };
        //     load_pais_prov_poblac.loadPoblacion(datos)
        //         .then(function (response) {
        //             if (response.success) {
        //                 $scope.poblaciones = response.datas;
        //             } else {
        //                 $scope.AlertMessage = true;
        //                 $scope.user.pob_error = "Error al recuperar la informacion de poblaciones";
        //                 $timeout(function () {
        //                     $scope.user.pob_error = "";
        //                     $scope.AlertMessage = false;
        //                 }, 2000);
        //             }
        //         });
        // };

    $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=login&function=upload_avatar',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function(file, formData, xhr) {},
            'success': function(file, response) {
                //console.log(response);
                response = JSON.parse(response);
                //console.log(response);
                if (response.resultado) {
                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
                    $('.msg').animate({ 'right': '300px' }, 300);

                    //console.log(response.datos);
                    $scope.avatar = response.datos;

                    // var user = {usuario: $scope.userInfo.nombre, avatar: response.datos, nombre: $scope.userInfo.nombre};
                    // cookiesService.SetCredentials(user);

                    loginService.login();
                } else {
                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    $('.msg').animate({ 'right': '300px' }, 300);
                }
            },
            'removedfile': function(file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("user", "delete_avatar", JSON.stringify({ 'filename': data }));
                }
            }
        }
    };

    $scope.submit = function() {
        // console.log($scope.profile_form);
        var user = localstorageService.get_id_user();

        if (!$scope.profile_form.selectProvincia) {
            provincia = "Not set";
        } else {
            provincia = $scope.profile_form.selectProvincia.nombre;
        }
        if (!$scope.profile_form.selectCity) {
            ciudad = "Not set";
        } else {
            ciudad = $scope.profile_form.selectCity.poblacion;
        }
        if (!$scope.avatar) {
            var data = { 'id_user': user, 'nombre': $scope.user.nombre, 'provincia': provincia, 'ciudad': ciudad }

        } else {

            var data = { 'id_user': user, 'nombre': $scope.user.nombre, 'avatar': $scope.avatar, 'provincia': provincia, 'ciudad': ciudad }

        }
        //   console.log(data);
        services.post('login', 'update_user', { 'total_data': JSON.stringify(data) }).then(function(response) {


            //console.log(response);

            if (response === 'true') {
                toastr.success('Datos actualizados correctamente', 'Perfecto', {
                    closeButton: true
                });
                location.href = '#/';
            }


        });

    }
    $scope.data = data;
    $scope.index = 0;
    $scope.names = data.nombre;
    //console.log(data);
    cont = 2
    $scope.products = data.slice(0, cont);
    //console.log($scope.products);

    $scope.filteredRestaurants = data;
    $scope.filteredPaint = $scope.filteredRestaurants.slice(0, 3);
    //console.log($scope.filteredPaint);

    $scope.showMore = function() {
        cont = cont + 2;
        $scope.filteredPaint = data.slice(0, cont);
        if (cont == 6) {
            var prov = document.querySelector('#click_scroll');
            prov.remove();
        }
    }
})