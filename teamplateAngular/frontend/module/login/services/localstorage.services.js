restaurants.factory("localstorageService", ['$timeout', '$filter', '$q', function($timeout, $filter, $q) {
	var service = {};
	service.getToken = getToken;
	service.getTipo = getTipo;
	service.setUsers = setUsers;
	service.setID_user = setID_user;
	service.set_Tipo = set_Tipo;
	service.get_id_user = get_id_user;
	service.clearUsers = clearUsers;
	service.setsocial = setsocial;
	service.getsocial = getsocial;
	return service;

	function getToken() {
		if ((!localStorage.token) || (localStorage.token == 'undefined')) {
			localStorage.token = JSON.stringify(false);
		}
		// console.log(localStorage.token);
		return JSON.parse(localStorage.token);
	}

	function setsocial(data) {
		localStorage.social = JSON.stringify(data);
	}

	function getsocial() {
		if (!localStorage.social) {
			localStorage.social = JSON.stringify(false);
		}
		return JSON.parse(localStorage.social);
	}

	function getTipo() {
		if (!localStorage.set_Tipo) {
			localStorage.set_Tipo = JSON.stringify(false);
		}

		return JSON.parse(localStorage.set_Tipo);
	}

	function get_id_user() {
		if (!localStorage.id_user) {
			localStorage.id_user = JSON.stringify(false);
		}
		return JSON.parse(localStorage.id_user);
	}

	function setUsers(token) {
		localStorage.token = JSON.stringify(token);
	}

	function setID_user(user) {
		localStorage.id_user = JSON.stringify(user);
	}

	function set_Tipo(tipo) {
		localStorage.set_Tipo = JSON.stringify(tipo);
	}

	function clearUsers() {
		localStorage.token = JSON.stringify(false);
		localStorage.id_user = JSON.stringify(false);
		localStorage.set_Tipo = JSON.stringify(false);
		localStorage.social = JSON.stringify(false);
	}


}]);