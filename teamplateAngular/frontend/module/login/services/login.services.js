restaurants.factory("loginService", ['$location', '$rootScope', 'services', 'localstorageService',
	function($location, $rootScope, services, localstorageService, socialService) {
		var service = {};
		service.login = login;
		service.logout = logout;
		return service;

		function login() {
			// console.log($rootScope);
			var token = localstorageService.getToken();
			var social = localstorageService.getsocial();
			if (token) {
				//depen del tipo de usuari pinta un menu o un altre
				var response = localstorageService.getTipo();
				// console.log(response);
				if (response === "user") {
					$rootScope.loginV = false;
					$rootScope.profileV = true;
					$rootScope.ubicaV = true;
					$rootScope.inicioV = true;
					$rootScope.logoutV = true;
					$rootScope.recoverV = true;
					$rootScope.shopV = true;
					$rootScope.crudV = false;


				} else if (response === 'admin') {
					$rootScope.loginV = false;
					$rootScope.profileV = true;
					$rootScope.ubicaV = true;
					$rootScope.inicioV = true;
					$rootScope.logoutV = true;
					$rootScope.recoverV = true;
					$rootScope.shopV = true;
					$rootScope.crudV = true;
				} else {
					$rootScope.loginV = true;
					$rootScope.logoutV = false;
					$rootScope.recoverV = false;
					$rootScope.shopV = false;
					$rootScope.crudV = false;

				}

			} else {
				$rootScope.loginV = true;
				$rootScope.shopV = false;

			}
			if (social == 'si') {
				$rootScope.recoverV = false;

			}
		}

		function logout() {
			localstorageService.clearUsers();
			$rootScope.loginV = true;
			$rootScope.logoutV = false;
			$rootScope.recoverV = false;
			$rootScope.shopV = false;
			$rootScope.crudV = false;
			$rootScope.profileV = false;

		}
	}
]);