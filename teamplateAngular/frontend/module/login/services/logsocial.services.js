restaurants.factory("socialService", ['$rootScope', 'services', 'localstorageService', '$timeout',
	function($rootScope, services, localstorageService, $timeout) {
		var service = {};
		service.initialize = initialize;
		//service.insertData = insertData;
		return service;

		function initialize() {
			var webAuth = new auth0.WebAuth({
				domain: 'vaoangel.eu.auth0.com',
				clientID: 'gPjRty1h045MuI0pXHIsLGmk1POru355',
				redirectUri: 'http://localhost/1DAW/teamplateAngular/#/login',
				audience: 'https://' + 'vaoangel.eu.auth0.com' + '/userinfo',
				responseType: 'token id_token',
				scope: 'openid profile',
				leeway: 60

			});
			return webAuth;
		}

		function handleAuthentication() {
			webAuth.parseHash(function(err, authResult) {
				if (authResult && authResult.accessToken && authResult.idToken) {
					window.location.hash = '';
					setSession(authResult);
					loginBtn.css('display', 'none');
					homeView.css('display', 'inline-block');
				} else if (err) {
					homeView.css('display', 'inline-block');
					//  console.log(err);
					alert('Error: ' + err.error + '. Check the console for further details.');
				}
				displayButtons();
			});
		}

		function displayProfile() {
			// display the profile
			//console.log("entra display");
			//console.log(userProfile);
			// $('#profile-view .nickname').text(userProfile.nickname);
			//$('#profile-view .full-profile').text(JSON.stringify(userProfile, null, 2));
			//$('#profile-view img').attr('src', userProfile.picture);
		}

	}
]);



restaurants.factory("googleService", ['$rootScope', 'services', 'socialService',
	function($rootScope, services, socialService) {
		var service = {};
		//service.login = login;
		return service;



	}
]);