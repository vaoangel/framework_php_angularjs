restaurants.controller('shopCtrl', function($scope, $route, data, services, localstorageService, $timeout) {
	testing = localstorageService.get_id_user();
	if (!testing) {
		location.href = "#/"

	}

	social = localstorageService.getsocial();
	//console.log(social);
// comproba si es social i reseteja token periodicament
	if (social != 'si') {
		setInterval(function() {
			testing = localstorageService.get_id_user();

			if (testing) {
				oldtoken = localstorageService.getToken();
				id_user = localstorageService.get_id_user();
				data = { 'oldtoken': oldtoken, 'id_user': id_user };
				services.post('home', 'timeout_token', { 'total_data': JSON.stringify(data) }).then(function(response) {
					console.log(response);
					console.log(data);
					if ((response.result1 == false) || (response.result2 == false) || (response.result3 == false)) {
						alert("ErrorToken");
					} else {
						localstorageService.setUsers(response.newtoken);
						console.log("works");
					}


					// pepe = localstorageService.getToken();
					// console.log(pepe);
				})
			} else {
				console.log("not loged");
			}



		}, 12000000);
	}
	$scope.data = data;
	$scope.index = 0;
	$scope.names = data.nombre;

	cont = 2
	$scope.products = data.slice(0, cont);
	//console.log($scope.products);

	$scope.filteredRestaurants = data;
	$scope.filteredPaint = $scope.filteredRestaurants.slice(0, 3);
	$scope.showMore = function() {
		cont = cont + 2;
		$scope.filteredPaint = data.slice(0, cont);
		if (cont == 6) {
			var prov = document.querySelector('#click_scroll');
			prov.remove();
		}
	}
})

restaurants.controller('reservaCtrl', function($scope, $route, id, services, $timeout, toastr, localstorageService) {
	$scope.id = id;

//efectua una reserva en BDD

	$scope.submitReserve = function() {
		// console.log($scope.reserve_form);
		user = localstorageService.get_id_user();
		var data = { 'usuario': user, 'restaurante': id, 'nombre_reserva': $scope.reserve.inputUser, 'email': $scope.reserve.inputEmail, 'fecha': $scope.reserve.dateR, 'hora': $scope.reserve_form.inputHour.$viewValue };
		//  console.log(data);

		services.post('home', 'insert_reserve', { 'total_data': JSON.stringify(data) }).then(function(response) {

			if (response == 'true') {
				toastr.success('Reserva realizada correctamente, Te hemos mandado un correo de recordatorio', 'Perfecto', {
					closeButton: true
				});

				location.href = "#/"
			}
		})
	}


})